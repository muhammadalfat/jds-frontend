import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as d3 from 'd3';
import { getJumlahJenisKendaraanPerTahun, JumlahJenisKendaraanPerTahun } from '../utils/api.sarana.unit.mobil';

interface LineChartData {
  tahun: string;
  total_jumlah_mobil: number;
}

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {
  private data: LineChartData[] = [];

  jumlahJenisKendaraanPerTahun: JumlahJenisKendaraanPerTahun = {
    status_code: 400,
    message: 'Failed',
    data: []
  };

  @ViewChild('svgContainer', { static: true }) svgContainer!: ElementRef;

  constructor() {}

  async ngOnInit(): Promise<void> {
    await this.fetchJumlahJenisKendaraanPerTahun();
    this.createChart();
  }

  private createChart(): void {
    const margin = { top: 20, right: 30, bottom: 60, left: 60 }; // Sesuaikan margin untuk label sumbu x dan y
    const width = 800 - margin.left - margin.right;
    const height = 400 - margin.top - margin.bottom;

    const svg = d3.select(this.svgContainer.nativeElement)
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', `translate(${margin.left},${margin.top})`);

    const x = d3.scaleBand()
      .domain(this.data.map(d => d.tahun))
      .range([0, width])
      .padding(1);

    const y = d3.scaleLinear()
      .domain([0, d3.max(this.data, d => d.total_jumlah_mobil) || 0])
      .nice()
      .range([height, 0]);

    svg.append('g')
      .attr('transform', `translate(0,${height})`)
      .call(d3.axisBottom(x));

    svg.append('g')
      .call(d3.axisLeft(y));

    for (let i = 0; i < this.data.length - 1; i++) {
      const d1 = this.data[i];
      const d2 = this.data[i + 1];

      svg.append('line')
        .attr('x1', x(d1.tahun) || 0)
        .attr('y1', y(d1.total_jumlah_mobil))
        .attr('x2', x(d2.tahun) || 0)
        .attr('y2', y(d2.total_jumlah_mobil))
        .attr('stroke', d2.total_jumlah_mobil >= d1.total_jumlah_mobil ? 'green' : 'red')
        .attr('stroke-width', 1.5);
    }

    const tooltip = d3.select('.chart-container').append('div')
      .attr('class', 'tooltip')
      .style('position', 'absolute')
      .style('padding', '8px')
      .style('background', 'rgba(0,0,0,0.6)')
      .style('border-radius', '4px')
      .style('color', '#fff')
      .style('opacity', 0);

    svg.selectAll('circle')
      .data(this.data)
      .enter()
      .append('circle')
      .attr('cx', d => x(d.tahun) || 0)
      .attr('cy', d => y(d.total_jumlah_mobil))
      .attr('r', 5)
      .attr('fill', 'steelblue')
      .on('mouseover', function(event, d) {
        d3.select(this)
          .transition()
          .duration(200)
          .attr('r', 8)
          .attr('fill', 'orange');
        tooltip.transition().duration(200).style('opacity', 1);
        tooltip.html(`Tahun: ${d.tahun}<br>Total Mobil: ${d.total_jumlah_mobil}`)
          .style('left', `${event.pageX + 10}px`)
          .style('top', `${event.pageY - 30}px`);
      })
      .on('mouseout', function(event, d) {
        d3.select(this)
          .transition()
          .duration(200)
          .attr('r', 5)
          .attr('fill', 'steelblue');
        tooltip.transition().duration(200).style('opacity', 0);
      });

    // Menambahkan teks di atas setiap titik
    svg.selectAll('.text')
      .data(this.data)
      .enter()
      .append('text')
      .attr('class', 'label')
      .attr('x', d => x(d.tahun) || 0)
      .attr('y', d => y(d.total_jumlah_mobil) - 10)
      .text(d => d.total_jumlah_mobil)
      .attr('text-anchor', 'middle')
      .style('font-size', '10px')
      .style('fill', 'black');


    // Teks "Jumlah kendaraan" secara vertikal di sebelah kiri sumbu y
    svg.append('text')
      .attr('transform', 'rotate(-90)')
      .attr('x', -height / 2)
      .attr('y', -margin.left)
      .attr('dy', '1em')
      .style('text-anchor', 'middle')
      .style('font-size', '14px')
      .text('Jumlah kendaraan');

    // Teks "Tahun" di bawah sumbu x
    svg.append('text')
      .attr('transform', `translate(${width / 2},${height + margin.top + 20})`)
      .style('text-anchor', 'middle')
      .style('font-size', '14px')
      .text('Tahun');
  }

  async fetchJumlahJenisKendaraanPerTahun() {
    try {
      this.jumlahJenisKendaraanPerTahun = await getJumlahJenisKendaraanPerTahun();
      this.data = this.jumlahJenisKendaraanPerTahun.data;

      if (this.jumlahJenisKendaraanPerTahun.status_code != 200) {
          console.log('error')
      }
    } catch (error) {
      console.error('Error fetching fetchJumlahJenisKendaraanPerTahun', error);
    }
  }
}
