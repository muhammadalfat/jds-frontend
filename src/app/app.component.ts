import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'YourApp';

  toggleChart(chartId: string): void {
    const chartContainer = document.getElementById(chartId + 'Container');
    if (chartContainer) {
      if (chartContainer.style.maxHeight === '0px') {
        chartContainer.style.maxHeight = chartContainer.scrollHeight + 'px';
      } else {
        chartContainer.style.maxHeight = '0px';
      }
    }
  }
}
