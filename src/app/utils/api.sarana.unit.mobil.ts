// src/app/utils/api.sarana.unit.mobil.ts

import apiService from './api.service';

const ENDPOINT_GROUP = '/api/v1/sarana_unit_mobil'; 

const API_GET_JUMLAH_JENIS_KENDARAAN = `${ENDPOINT_GROUP}/jumlah_jenis_kendaaran`; 
const API_GET_JUMLAH_JENIS_KENDARAAN_PER_TAHUN = `${ENDPOINT_GROUP}/jumlah_jenis_kendaaran_per_tahun`; 
const API_GET_JUMLAH_DETAIL_JENIS_KENDARAAN_PERTAHUN = `${ENDPOINT_GROUP}/jumlah_detail_jenis_kendaaran_per_tahun`; 

export interface JumlahJenisKendaraanData {
  jenis_kendaraan: string;
  total_jumlah_mobil: number; 
}

export interface JumlahJenisKendaraan {
  status_code: number,
  message: string,
  data: {
    jenis_kendaraan: string;
    total_jumlah_mobil: number;
  }[]
}

export interface JumlahJenisKendaraanPerTahun {
  status_code: number,
  message: string,
  data: {
    tahun: string;
    total_jumlah_mobil: number;
  }[]
}

export interface JumlahDetailJenisKendaraanPerTahun {
  status_code: number,
  message: string,
  data: {
    jenis_kendaraan: string;
    tahun: string;
    total_jumlah_mobil: number;
  }[][]
}

export const getJumlahJenisKendaraan = async (): Promise<JumlahJenisKendaraan> => {
  try {
    const response = await apiService.get<JumlahJenisKendaraan>(API_GET_JUMLAH_JENIS_KENDARAAN);
    return response.data;
  } catch (error) {
    console.error('Error fetching getJumlahJenisKendaraan', error);
    throw error;
  }
};


export const getJumlahJenisKendaraanPerTahun = async (): Promise<JumlahJenisKendaraanPerTahun> => {
    try {
      const response = await apiService.get<JumlahJenisKendaraanPerTahun>(API_GET_JUMLAH_JENIS_KENDARAAN_PER_TAHUN);
      return response.data;
    } catch (error) {
      console.error('Error fetching getJumlahJenisKendaraanPerTahun', error);
      throw error;
    }
  };
  

  export const getJumlahDetailJenisKendaraanPerTahun = async (): Promise<JumlahDetailJenisKendaraanPerTahun> => {
    try {
      const response = await apiService.get<JumlahDetailJenisKendaraanPerTahun>(API_GET_JUMLAH_DETAIL_JENIS_KENDARAAN_PERTAHUN+'?return_type=list');
      return response.data;
    } catch (error) {
      console.error('Error fetching JumlahDetailJenisKendaraanPerTahun', error);
      throw error;
    }
  };


  export const getJumlahDetailJenisKendaraanPerTahunList = async (): Promise<JumlahDetailJenisKendaraanPerTahun> => {
    try {
      const response = await apiService.get<JumlahDetailJenisKendaraanPerTahun>(API_GET_JUMLAH_DETAIL_JENIS_KENDARAAN_PERTAHUN+'?return_type=object_list');
      return response.data;
    } catch (error) {
      console.error('Error fetching JumlahDetailJenisKendaraanPerTahunList', error);
      throw error;
    }
  };
  
  