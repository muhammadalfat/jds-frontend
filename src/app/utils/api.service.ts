// src/app/utils/api.service.ts

import axios, { AxiosInstance, AxiosResponse } from 'axios';

class ApiService {
  private axiosInstance: AxiosInstance;

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: 'http://localhost:8085', // Replace with your API base URL
      headers: {
        'Content-Type': 'application/json',
        'X-API-KEY': 'da!31as~1221sdax!wsda!@!``wsdasdazcZXCQwae$!$$%%11d2133$=-i76543rdsfds'
      },
    });

    // Interceptors can be added here if needed
    this.axiosInstance.interceptors.request.use(
      config => {
        // Add any custom logic or headers
        return config;
      },
      error => {
        return Promise.reject(error);
      }
    );

    this.axiosInstance.interceptors.response.use(
      response => response,
      error => {
        return Promise.reject(error);
      }
    );
  }

  async get<T>(url: string): Promise<AxiosResponse<T>> {
    return this.axiosInstance.get<T>(url);
  }

  async post<T>(url: string, data: T): Promise<AxiosResponse<T>> {
    return this.axiosInstance.post<T>(url, data);
  }

  async put<T>(url: string, data: T): Promise<AxiosResponse<T>> {
    return this.axiosInstance.put<T>(url, data);
  }

  async delete<T>(url: string): Promise<AxiosResponse<T>> {
    return this.axiosInstance.delete<T>(url);
  }
}

const apiService = new ApiService();
export default apiService;
