import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
import { JumlahJenisKendaraan, getJumlahJenisKendaraan } from '../utils/api.sarana.unit.mobil';


interface PieChartData {
  jenis_kendaraan: string;
  total_jumlah_mobil: number;
}


@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {

  private data: PieChartData[] = [];

  constructor() { }

  jumlahJenisKendaraan: JumlahJenisKendaraan = {
    status_code: 400,
    message: 'Failed',
    data: []
  };

  async ngOnInit(): Promise<void> {
    await this.fetchJumlahJenisKendaraan();
      this.createPieChart();
  }


  private createPieChart(): void {
    const jumlah_jenis_kendaraan = this.data;
  
    const data = jumlah_jenis_kendaraan.map(d => d.total_jumlah_mobil);
    const total = data.reduce((acc, val) => acc + val, 0);
  

    const margin = { top: 20, right: 180, bottom: 40, left: 50 };
    const width = 800 - margin.left - margin.right;
    const height = 550 - margin.top - margin.bottom;

    const radius = Math.min(width, height) / 2 - 20;
    const innerRadius = radius / 2;
  
    const color = d3.scaleOrdinal(d3.schemeCategory10);
  
    const pie = d3.pie();
    const arc = d3.arc()
      .innerRadius(innerRadius)
      .outerRadius(radius);
  
    const svg = d3.select('#pieChart')
      .append('svg')
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', `translate(${width / 2}, ${height / 2})`);
  
    const arcs = pie(data);
  
    const tooltip = d3.select('body').append('div')
      .attr('class', 'tooltip')
      .style('position', 'absolute')
      .style('padding', '8px')
      .style('background', 'rgba(0,0,0,0.6)')
      .style('border-radius', '4px')
      .style('color', '#fff')
      .style('visibility', 'hidden'); // Initially hidden
  
    // Add the arcs
    svg.selectAll('path')
      .data(arcs)
      .enter().append('path')
      .attr('fill', (d, i) => color(i.toString()))
      .attr('d', arc as any)
      .attr('stroke', 'white')
      .attr('stroke-width', '2px')
      .on('mouseover', function (event, d) {
        // Darken all arcs except the hovered one
        svg.selectAll('path')
          .transition()
          .duration(200)
          .style('opacity', arcData => (arcData === d ? 1 : 0.5));
  
        // Show tooltip
        tooltip.html(`Jenis Kendaraan: ${jumlah_jenis_kendaraan[d.index].jenis_kendaraan}<br>Percentage: ${((d.value / total) * 100).toFixed(2)}%`)
          .style('visibility', 'visible');
      })
      .on('mousemove', function (event) {
        tooltip.style('top', (event.pageY - 10) + 'px')
          .style('left', (event.pageX + 10) + 'px');
      })
      .on('mouseout', function (event, d) {
        // Restore all arcs to normal
        svg.selectAll('path')
          .transition()
          .duration(200)
          .style('opacity', 1);
  
        // Hide tooltip
        tooltip.style('visibility', 'hidden');
      });
  
    // Add labels to the pie chart slices
    svg.selectAll('text')
      .data(arcs)
      .enter().append('text')
      .attr('transform', d => `translate(${arc.centroid(d as any)})`)
      .attr('text-anchor', 'middle')
      .text((d, i) => {
        const percentage = ((d.value / total) * 100).toFixed(2);
        return `${jumlah_jenis_kendaraan[i].jenis_kendaraan}: ${percentage}%`;
      })
      .style('fill', 'white')
      .style('font-size', '10px')
      .style('font-family', 'Arial')
      .style('pointer-events', 'none');
  
    // Add legend
    const legend = svg.append('g')
      .attr('transform', `translate(${width / 2}, ${-height / 2 + 20})`)  // Posisikan di pojok kanan atas
      .style('font-size', '10px')
      .style('font-family', 'Arial')

    jumlah_jenis_kendaraan.forEach((d, i) => {
      const legendRow = legend.append('g')
        .attr('transform', `translate(0, ${i * 20})`);
  
      legendRow.append('rect')
        .attr("width", 18)
        .attr("height", 18)
        .attr('fill', color(i.toString()));
  
      const legendText = legendRow.append('text')
        .attr('x', 22)
        .attr('y', 12)
        .attr('text-anchor', 'start')
        .style('text-transform', 'capitalize')
        .text(d.jenis_kendaraan);
  
      // Add hover events to legend text
      legendText.on('mouseover', function (event) {
        // Darken the corresponding arc
        svg.selectAll('path')
          .transition()
          .duration(200)
          .style('opacity', (arcData, j) => (j === i ? 1 : 0.5));
  
        // Show tooltip
        tooltip.html(`Jenis Kendaraan: ${jumlah_jenis_kendaraan[i].jenis_kendaraan}<br>Percentage: ${((data[i] / total) * 100).toFixed(2)}%`)
          .style('visibility', 'visible');
      })
      .on('mousemove', function (event) {
        tooltip.style('top', (event.pageY - 10) + 'px')
          .style('left', (event.pageX + 10) + 'px');
      })
      .on('mouseout', function () {
        // Restore all arcs to normal
        svg.selectAll('path')
          .transition()
          .duration(200)
          .style('opacity', 1);
  
        // Hide tooltip
        tooltip.style('visibility', 'hidden');
      });
    });
  }
  


  async fetchJumlahJenisKendaraan() {
    try {
      this.jumlahJenisKendaraan = await getJumlahJenisKendaraan();

      if (this.jumlahJenisKendaraan.status_code != 200) {
          console.log('error')
      }

      this.data = this.jumlahJenisKendaraan.data;


    } catch (error) {
      console.error('Error fetching fetchJumlahJenisKendaraan', error);
    }
  }
}
