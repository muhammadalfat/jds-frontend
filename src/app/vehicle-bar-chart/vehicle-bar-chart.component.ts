import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';

interface VehicleData {
  jenis_kendaraan: string;
  tahun: string;
  total_jumlah_mobil: number;
}

@Component({
  selector: 'app-vehicle-bar-chart',
  templateUrl: './vehicle-bar-chart.component.html',
  styleUrls: ['./vehicle-bar-chart.component.css']
})
export class VehicleBarChartComponent implements OnInit {

  private data: VehicleData[][] = [
    [
      {
        "jenis_kendaraan": "ANGKUTAN OPERASIONAL",
        "tahun": "2013",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "ANGKUTAN PERSONIL",
        "tahun": "2013",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "KOMANDO",
        "tahun": "2013",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "OPERASIONAL BENGKEL",
        "tahun": "2013",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "PEMADAMAN",
        "tahun": "2013",
        "total_jumlah_mobil": 17
      },
      {
        "jenis_kendaraan": "PENCEGAHAN",
        "tahun": "2013",
        "total_jumlah_mobil": 1
      },
      {
        "jenis_kendaraan": "PENDUKUNG",
        "tahun": "2013",
        "total_jumlah_mobil": 2
      },
      {
        "jenis_kendaraan": "PENYELAMATAN",
        "tahun": "2013",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "RESCUE",
        "tahun": "2013",
        "total_jumlah_mobil": 6
      }
    ],
    [
      {
        "jenis_kendaraan": "ANGKUTAN OPERASIONAL",
        "tahun": "2014",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "ANGKUTAN PERSONIL",
        "tahun": "2014",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "KOMANDO",
        "tahun": "2014",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "OPERASIONAL BENGKEL",
        "tahun": "2014",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "PEMADAMAN",
        "tahun": "2014",
        "total_jumlah_mobil": 20
      },
      {
        "jenis_kendaraan": "PENCEGAHAN",
        "tahun": "2014",
        "total_jumlah_mobil": 1
      },
      {
        "jenis_kendaraan": "PENDUKUNG",
        "tahun": "2014",
        "total_jumlah_mobil": 5
      },
      {
        "jenis_kendaraan": "PENYELAMATAN",
        "tahun": "2014",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "RESCUE",
        "tahun": "2014",
        "total_jumlah_mobil": 6
      }
    ],
    [
      {
        "jenis_kendaraan": "ANGKUTAN OPERASIONAL",
        "tahun": "2015",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "ANGKUTAN PERSONIL",
        "tahun": "2015",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "KOMANDO",
        "tahun": "2015",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "OPERASIONAL BENGKEL",
        "tahun": "2015",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "PEMADAMAN",
        "tahun": "2015",
        "total_jumlah_mobil": 20
      },
      {
        "jenis_kendaraan": "PENCEGAHAN",
        "tahun": "2015",
        "total_jumlah_mobil": 1
      },
      {
        "jenis_kendaraan": "PENDUKUNG",
        "tahun": "2015",
        "total_jumlah_mobil": 7
      },
      {
        "jenis_kendaraan": "PENYELAMATAN",
        "tahun": "2015",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "RESCUE",
        "tahun": "2015",
        "total_jumlah_mobil": 7
      }
    ],
    [
      {
        "jenis_kendaraan": "ANGKUTAN OPERASIONAL",
        "tahun": "2016",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "ANGKUTAN PERSONIL",
        "tahun": "2016",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "KOMANDO",
        "tahun": "2016",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "OPERASIONAL BENGKEL",
        "tahun": "2016",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "PEMADAMAN",
        "tahun": "2016",
        "total_jumlah_mobil": 22
      },
      {
        "jenis_kendaraan": "PENCEGAHAN",
        "tahun": "2016",
        "total_jumlah_mobil": 1
      },
      {
        "jenis_kendaraan": "PENDUKUNG",
        "tahun": "2016",
        "total_jumlah_mobil": 8
      },
      {
        "jenis_kendaraan": "PENYELAMATAN",
        "tahun": "2016",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "RESCUE",
        "tahun": "2016",
        "total_jumlah_mobil": 7
      }
    ],
    [
      {
        "jenis_kendaraan": "ANGKUTAN OPERASIONAL",
        "tahun": "2017",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "ANGKUTAN PERSONIL",
        "tahun": "2017",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "KOMANDO",
        "tahun": "2017",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "OPERASIONAL BENGKEL",
        "tahun": "2017",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "PEMADAMAN",
        "tahun": "2017",
        "total_jumlah_mobil": 23
      },
      {
        "jenis_kendaraan": "PENCEGAHAN",
        "tahun": "2017",
        "total_jumlah_mobil": 1
      },
      {
        "jenis_kendaraan": "PENDUKUNG",
        "tahun": "2017",
        "total_jumlah_mobil": 11
      },
      {
        "jenis_kendaraan": "PENYELAMATAN",
        "tahun": "2017",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "RESCUE",
        "tahun": "2017",
        "total_jumlah_mobil": 7
      }
    ],
    [
      {
        "jenis_kendaraan": "ANGKUTAN OPERASIONAL",
        "tahun": "2019",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "ANGKUTAN PERSONIL",
        "tahun": "2019",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "KOMANDO",
        "tahun": "2019",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "OPERASIONAL BENGKEL",
        "tahun": "2019",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "PEMADAMAN",
        "tahun": "2019",
        "total_jumlah_mobil": 24
      },
      {
        "jenis_kendaraan": "PENCEGAHAN",
        "tahun": "2019",
        "total_jumlah_mobil": 1
      },
      {
        "jenis_kendaraan": "PENDUKUNG",
        "tahun": "2019",
        "total_jumlah_mobil": 28
      },
      {
        "jenis_kendaraan": "PENYELAMATAN",
        "tahun": "2019",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "RESCUE",
        "tahun": "2019",
        "total_jumlah_mobil": 7
      }
    ],
    [
      {
        "jenis_kendaraan": "ANGKUTAN OPERASIONAL",
        "tahun": "2020",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "ANGKUTAN PERSONIL",
        "tahun": "2020",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "KOMANDO",
        "tahun": "2020",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "OPERASIONAL BENGKEL",
        "tahun": "2020",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "PEMADAMAN",
        "tahun": "2020",
        "total_jumlah_mobil": 25
      },
      {
        "jenis_kendaraan": "PENCEGAHAN",
        "tahun": "2020",
        "total_jumlah_mobil": 3
      },
      {
        "jenis_kendaraan": "PENDUKUNG",
        "tahun": "2020",
        "total_jumlah_mobil": 25
      },
      {
        "jenis_kendaraan": "PENYELAMATAN",
        "tahun": "2020",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "RESCUE",
        "tahun": "2020",
        "total_jumlah_mobil": 7
      }
    ],
    [
      {
        "jenis_kendaraan": "ANGKUTAN OPERASIONAL",
        "tahun": "2021",
        "total_jumlah_mobil": 1
      },
      {
        "jenis_kendaraan": "ANGKUTAN PERSONIL",
        "tahun": "2021",
        "total_jumlah_mobil": 1
      },
      {
        "jenis_kendaraan": "KOMANDO",
        "tahun": "2021",
        "total_jumlah_mobil": 12
      },
      {
        "jenis_kendaraan": "OPERASIONAL BENGKEL",
        "tahun": "2021",
        "total_jumlah_mobil": 2
      },
      {
        "jenis_kendaraan": "PEMADAMAN",
        "tahun": "2021",
        "total_jumlah_mobil": 23
      },
      {
        "jenis_kendaraan": "PENCEGAHAN",
        "tahun": "2021",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "PENDUKUNG",
        "tahun": "2021",
        "total_jumlah_mobil": 0
      },
      {
        "jenis_kendaraan": "PENYELAMATAN",
        "tahun": "2021",
        "total_jumlah_mobil": 9
      },
      {
        "jenis_kendaraan": "RESCUE",
        "tahun": "2021",
        "total_jumlah_mobil": 0
      }
    ]
  ];

  constructor() { }

  ngOnInit(): void {
    this.createChart();
  }

  private createChart(): void {
    const margin = { top: 20, right: 180, bottom: 40, left: 50 };
    const width = 800 - margin.left - margin.right;
    const height = 400 - margin.top - margin.bottom;

    const svg = d3.select("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", `translate(${margin.left},${margin.top})`);

    // Flatten the data
    const flattenedData = this.data.flat();

    // Group data by year
    const years = [...new Set(flattenedData.map(d => d.tahun))];
    const categories = [...new Set(flattenedData.map(d => d.jenis_kendaraan))];

    // Set up the scales
    const x0 = d3.scaleBand()
      .domain(years)
      .range([0, width])
      .padding(0.1);

    const x1 = d3.scaleBand()
      .domain(categories)
      .range([0, x0.bandwidth()])
      .padding(0.05);

    const y = d3.scaleLinear()
      .domain([0, d3.max(flattenedData, d => d.total_jumlah_mobil) || 0])
      .nice()
      .range([height, 0]);

    // Color scale for different jenis_kendaraan
    const color = d3.scaleOrdinal(d3.schemeCategory10)
      .domain(categories);

    // Add the X Axis
    svg.append("g")
      .attr("transform", `translate(0,${height})`)
      .call(d3.axisBottom(x0).tickFormat(d => d.toString()));

    // Add the Y Axis
    svg.append("g")
      .call(d3.axisLeft(y));

    // Tooltip
    const tooltip = d3.select('body').append('div')
      .attr('class', 'tooltip')
      .style('position', 'absolute')
      .style('padding', '8px')
      .style('background', 'rgba(0,0,0,0.6)')
      .style('border-radius', '4px')
      .style('color', '#fff')
      .style('opacity', 0);

    // Add the bars
    const yearGroups = svg.selectAll(".year-group")
      .data(flattenedData)
      .enter().append("g")
      .attr("class", "year-group")
      .attr("transform", d => `translate(${x0(d.tahun)},0)`);

    yearGroups.selectAll(".bar")
      .data(d => categories.map(key => ({ key, value: d.total_jumlah_mobil, tahun: d.tahun, jenis_kendaraan: d.jenis_kendaraan })))
      .enter().append("rect")
      .attr("class", "bar")
      .attr("x", d => x1(d.jenis_kendaraan) || 0)
      .attr("y", d => y(d.value))
      .attr("width", x1.bandwidth())
      .attr("height", d => height - y(d.value))
      .attr("fill", d => color(d.jenis_kendaraan))
      .on("mouseover", function(event, d) {
        tooltip.transition()
          .duration(200)
          .style("opacity", 1);
        tooltip.html(`Jenis Kendaraan: ${d.jenis_kendaraan}<br>Jumlah: ${d.value}`)
          .style("left", (event.pageX + 5) + "px")
          .style("top", (event.pageY - 28) + "px");
      })
      .on("mouseout", function() {
        tooltip.transition()
          .duration(500)
          .style("opacity", 0);
      });

    // Add legend
    const legend = svg.selectAll(".legend")
      .data(categories)
      .enter().append("g")
      .attr("class", "legend")
      .attr("transform", (d, i) => `translate(0,${i * 20})`);

    legend.append("rect")
      .attr("x", width + 20)
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", color);

    legend.append("text")
      .attr("x", width + 45)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "start")
      .text(d => d);


    // Teks "Jumlah kendaraan" secara vertikal di sebelah kiri sumbu y
    svg.append('text')
      .attr('transform', 'rotate(-90)')
      .attr('x', -height / 2)
      .attr('y', -margin.left)
      .attr('dy', '1em')
      .style('text-anchor', 'middle')
      .style('font-size', '12px')
      .text('Jumlah kendaraan');

    // Teks "Tahun" di bawah sumbu x
    svg.append('text')
      .attr('transform', `translate(${width / 2},${height + margin.top + 20})`)
      .style('text-anchor', 'middle')
      .style('font-size', '12px')
      .text('Tahun');
  }
  
}
