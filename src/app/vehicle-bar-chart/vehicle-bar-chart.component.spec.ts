import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleBarChartComponent } from './vehicle-bar-chart.component';

describe('VehicleBarChartComponent', () => {
  let component: VehicleBarChartComponent;
  let fixture: ComponentFixture<VehicleBarChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VehicleBarChartComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VehicleBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
